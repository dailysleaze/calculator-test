<?php

namespace App;

interface ConverterInterface {

	public function convert(string $string) : string;

}
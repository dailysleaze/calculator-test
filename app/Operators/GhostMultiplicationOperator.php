<?php

namespace App\Operators;

class GhostMultiplicationOperator extends BaseOperator implements OperatorInterface {

	public $unicodeValue = 'U+1F47B';

	/**
	 * Multiplication calculation
	 *
	 * @param float $operand1
	 * @param float $operand2
	 * @return float
	 */
	public function process(float $operand1, float $operand2) : float {
		return $operand1 * $operand2;
	}

}
<?php

namespace App\Operators;

interface OperatorInterface {

	public function process(float $operand1, float $operand2) : float;

}
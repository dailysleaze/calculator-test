<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\{ConverterInterface, UnicodeToHtmlConverter};

class ConverterServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			ConverterInterface::class,
			UnicodeToHtmlConverter::class
		);
	}

}
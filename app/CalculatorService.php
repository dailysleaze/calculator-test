<?php

namespace App;

use App\Exceptions\UnknownOperatorException;

class CalculatorService implements CalculatorInterface {

	protected $operators;

	/**
	 *
	 * @param CalculatorInterface $calculator
	 * @param ConverterInterface $converter
	 */
	public function __construct($operators) {
		$this->operators = $operators;
	}

	/**
	 * An array of the operators emoji values that the calculator uses
	 *
	 * @param void
	 * @return array
	 */
	public function getOperatorList() : array {
		return array_column($this->operators, 'unicodeValue');
	}

	/**
	 * Produce the arithmetic result for the operands and operator given
	 *
	 * @param float $operand1
	 * @param float $operand2
	 * @param string $operatorValue
	 * @return float
	 */
	public function calculate(float $operand1, float $operand2, string $operatorValue) : float {

		//Find the operator we're using by the emoji code
		foreach ($this->operators as $operator) {
			if ($operatorValue == $operator->unicodeValue) {
				return $operator->process($operand1, $operand2);
			}
		}

		throw new UnknownOperatorException;
	}

}
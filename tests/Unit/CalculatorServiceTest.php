<?php

namespace Tests\Unit;

use Tests\TestCase;
use App;

class CalculatorServiceTest extends TestCase {

	public function testAdditionOperatorDoesAddition() {
		$calculator = App::make('App\CalculatorInterface');

		$result = $calculator->calculate(3, 4, 'U+1F47D');

		$this->assertEquals($result, 7);
	}

	public function testSubtractionOperatorDoesSubtraction() {
		$calculator = App::make('App\CalculatorInterface');

		$result = $calculator->calculate(3, 4, 'U+1F480');

		$this->assertEquals($result, -1);
	}

	public function testMultiplicationOperatorDoesMultiplication() {
		$calculator = App::make('App\CalculatorInterface');

		$result = $calculator->calculate(3, 4, 'U+1F47B');

		$this->assertEquals($result, 12);
	}

	public function testDivisionOperatorDoesDivision() {
		$calculator = App::make('App\CalculatorInterface');

		$result = $calculator->calculate(3, 4, 'U+1F631');

		$this->assertEquals($result, 0.75);
	}

	/**
	* @expectedException Exception
	*/
	public function testDivisionOperatorDivideZeroThrowsException() {
		$calculator = App::make('App\CalculatorInterface');

		$result = $calculator->calculate(3, 0, 'U+1F631');
	}

	/**
	* @expectedException App\Exceptions\UnknownOperatorException
	*/
	public function testUnknownOperatorException() {
		$calculator = App::make('App\CalculatorInterface');

		$calculator->calculate(3, 4, '1234');
	}

	/**
	* @expectedException TypeError
	*/
	public function testStringOperandException() {
		$calculator = App::make('App\CalculatorInterface');

		$calculator->calculate('testing', 4, 'U+1F47D');
	}

	/**
	* @expectedException TypeError
	*/
	public function testNullOperandException() {
		$calculator = App::make('App\CalculatorInterface');

		$calculator->calculate(3, null, 'U+1F47D');
	}
}

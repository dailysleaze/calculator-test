<?php

namespace App\Http\Controllers;

use App\{CalculatorInterface, ConverterInterface};
use App\Exceptions\UnknownOperatorException;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class CalculateController extends Controller {

	/**
	 * @param CalculatorInterface $calculator
	 * @param ConverterInterface $converter
	 */
	public function __construct(CalculatorInterface $calculator, ConverterInterface $converter) {
		$this->calculator = $calculator;
		$this->converter = $converter;
	}

	/**
	 * Show the calculator
	 *
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function index() {

		//Get all of the operators that are allowed to be used, for the dropdown list of options
		$operators = $this->calculator->getOperatorList();

		//Convert all the Unicode strings to the HTML character to display in the select list
		$values = array_map(function($operator) {
			return $this->converter->convert($operator);
		}, $operators);

		//Key the array by the unicode characters
		$operators = array_combine($operators, $values);

		return view('index', compact('operators'));
	}


	/**
	 * Calculate and return the arithmetic result from 2 operands and an operator
	 *
	 * @param Illuminate\Http\Request $request
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function calculate(Request $request) {

		$this->validate($request, [
			'operand1' => 'required|numeric',
			'operand2' => 'required|numeric',
			'operator' => 'required',
		]);

		//Send the data off to get the result
		try {
			$result = $this->calculator->calculate(
				Input::get('operand1'),
				Input::get('operand2'),
				Input::get('operator')
			);

		} catch (UnknownOperatorException $e) {
			$result = 'An unknown operator has been used';

		} catch (Exception $e) {
			$result = $e->getMessage();
		}

		return view('result', compact('result'));
	}

}
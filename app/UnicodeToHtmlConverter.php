<?php

namespace App;

class UnicodeToHtmlConverter implements ConverterInterface {

	/**
	 * E.g. converting U+1F47D to 128125 then translate into an HTML entity
	 *
	 * @param string $string
	 * @return string
	 */
	public function convert(string $string) : string {
		return "&#" . hexdec($string) . ";";
	}

}
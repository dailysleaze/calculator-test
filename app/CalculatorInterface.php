<?php

namespace App;

interface CalculatorInterface {

	public function getOperatorList() : array;

	public function calculate(float $operand1, float $operand2, string $operator) : float;

}
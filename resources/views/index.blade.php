<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Calculator</title>
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<style type="text/css">
			form {width:800px;margin:40px auto;}
			option, select {font-size: 3em;}
		</style>

		<script type="text/javascript">
			$(document).on('submit', 'form', function() {

				var $this = $(this);
				var url = $this.attr('action');
				var data = $this.serialize();

				$.ajax({
					type: "post",
					url: url,
					data: data,

				})
				.done(function(data){
					$('#result').html(data);
				})
				.fail(function(data){
					var validation = $.parseJSON(data.responseText);
					var str = '';

					$.each(validation, function(i, value) {
						str += value[0] + ' ';
					})
					$('#result').html(str);
				});

				return false;
			});
		</script>
	</head>
	<body>

		<form method="post" action="/calculate" class="form-inline">
			<h1>Calculatorizr.io</h1>
			<div class="form-group">
				<input type="text" name="operand1">
			</div>

			<div class="form-group">
				<select name="operator" class="form-control">
					@foreach ($operators as $operator => $value)
						<option value="{{ $operator }}">{{ $value }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<input type="text" name="operand2">
			</div>

			<button class="btn btn-default" type="submit">Calculate</button>

			<div>
				<h3>Result</h3>
				<div id="result" class="panel panel-default"></div>
			</div>
		</form>

	</body>
</html>

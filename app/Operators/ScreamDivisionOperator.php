<?php

namespace App\Operators;

use Exception;

class ScreamDivisionOperator extends BaseOperator implements OperatorInterface {

	public $unicodeValue = 'U+1F631';

	/**
	 * Division calculation
	 *
	 * @param float $operand1
	 * @param float $operand2
	 * @return float
	 */
	public function process(float $operand1, float $operand2) : float {

		if ($operand2 == 0) throw new Exception('Division by zero is not allowed');

		return $operand1 /  $operand2;
	}

}
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Operators\{AlienAdditionOperator, SkullSubtractionOperator, GhostMultiplicationOperator, ScreamDivisionOperator};
use App\CalculatorInterface;

class CalculatorServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		/**
		 * Inject the operators available in the calculator
		 */
		$this->app->bind(
			CalculatorInterface::class,
			function($app) {
				return new \App\CalculatorService([
					new AlienAdditionOperator,
					new SkullSubtractionOperator,
					new GhostMultiplicationOperator,
					new ScreamDivisionOperator,
				]);
			}
		);
	}

}
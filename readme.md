##Installation

- Run `composer install`
- Serve via the public folder `php -S localhost:8000 -t public/`

## Structure

The code is based on Laravel 5.4, as it is familiar to me and gave me a quick route to writing functionality. The code I have written resides in the following locations:

- app/*
- app/Exceptions/BaseException.php
- app/Exceptions/UnknownOperatorException.php
- app/Http/Controllers/CalculatorController.php
- app/Operators/*
- app/Providers/CalculatorServiceProvider.php
- app/Providers/ConverterServiceProvider.php
- resources/views/*
- tests/Unit/*

Every class has an interface and some have an abstract base class to allow extension in the future. Some newer features of PHP7 that I have specifically included are grouped namespaces, type hinted parameters and return values, which allow for a stricter codebase. I did not use to choose traits as there was no instance of multiple inheritance.

Placing the calculation logic in a separate class means it is decoupled from the controller and therefore the data delivery method. The dependencies are then injected via the ServiceProviders, where the interface is bound to the implementation. This means the implementation can be changed via a single line of code and can be easily mocked for testing. The operators are injected via the service provider so that different emojis can be swapped in and out easily, however they are not injected as an interface so that it doesn't restrict the application to a single emoji per operator. The class is also responsible for suppling a list of the appropriate operators to the controller to display, so that it doesn't leak into a different layer of abstraction.

I have also decoupled the translation of the Unicode emojis to HTML entities, which would allow a change in the type of string used to identify each emoji uniquely. If part of a more complex system I would have used [an open-source package](https://packagist.org/packages/emojione/emojione) to harness cross-browser compatible output and a flexible way of storing emojis in the database via short codes.

##Testing

To run the tests, execute `php vendor/phpunit/phpunit/phpunit`

The tests focus on ensuring the core logic remains the same for the given emojis and that exceptions are properly thrown.

##New emojis
To add different emoji operators, you would create a new Operator class and the enter it into the bindings in the `CalculatorServiceProvider` class.

##Improvements
To make this a production site, I would focus on removing any inline CSS and JS, as well as minification and gzipping. I would also look at making all AJAX responses JSON to make it a consistent API.